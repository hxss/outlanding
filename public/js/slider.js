
Element.prototype.newSlider = function(settings, ...args) {
	return new Slider(this, settings, args);
};
$.fn.slider = function(settings, ...args) {
	return $(this).map(function() {
		return this.newSlider(settings, ...args);
	});
};

function Slider(node, settings, args) {
	if (node.slider)
		return node.slider;

	this.classes = {
		this:  'slider',
		slide: 'slider__slide',
		arrow: 'slider__arrow',
		dot:   'slider__dot',
		wrappers: {
			slides: 'slider__slides_wrapper',
			arrows: 'slider__arrows_wrapper',
			dots:   'slider__dots_wrapper',
		},
	};
	this.settings = Object.assign({
		loop: true,
		arrows: true,
		dots: true,
		dotsHover: false,
		sync: [],
		events: {},
	}, settings);

	this.nodes = {
		wrappers: {},
	};
	this.slides = [];
	this.active = null;


//###########################
//### S L I D E R ###########
//###########################

	this.next = function() {
		return this.active.next().activate();
	};

	this.prev = function() {
		return this.active.prev().activate();
	};

	this.activate = function(id) {
		if ((id || id === 0)
			&& (this.active
				&& id != this.active.id
				|| this.active === null
			) && this.slides[id]
		) {
			this.deactivate();
			$(this.slides[id].node).addClass(this.class('slide+active'));
			this.active = this.slides[id];

			this.settings.sync.forEach(function(slider) {
				slider.activate(id);
			});

			return this;
		}

		return false;
	};

	this.deactivate = function() {
		if (this.active) {
			$(this.active.node).removeClass(this.class('slide+active'));
			this.active = null;

			this.settings.sync.forEach(function(slider) {
				slider.deactivate();
			});
		}
	};

//###########################
//### I N I T I A L I Z E ###
//###########################

	this.init = function(node, imgs) {
		$(node).addClass(this.class('this'));
		this.nodes.this = node;
		this.nodes.this.slider = this;

		this.initSlides();
		this.initArrows();
		this.initDots();
		this.initSync();

		this.activate(0);

		return this;
	};

	this.initSlides = function() {
		var childs = $(this.nodes.this).children();

		var wrapper = this.nodes.wrappers.slides
		            = $('<div></div>').addClass(this.class('wrappers.slides'));
		$(this.nodes.this).append(wrapper);

		childs.toArray().forEach(function(slideNode) {
			$(wrapper).append(slideNode);
			this.newSlide(slideNode);
		}, this);
	};

	this.initArrows = function() {
		if (this.settings.arrows) {
			this.nodes.arrows = {};
			var _this = this;
			var types = ['prev', 'next'];

			var wrapper = this.nodes.wrappers.arrows
			            = $('<div></div>').addClass(this.class('wrappers.arrows'));
			$(this.nodes.this).append(wrapper);

			types.forEach(function(type) {
				this.nodes.arrows[type] = $('<div></div>')
					.addClass(this.class('arrow'))
					.addClass(this.class('arrow+' + type))
					.click(function() {_this[type]();})
					.appendTo(wrapper)[0];
			}, this);
		}
	};

	this.initDots = function() {
		if (this.settings.dots) {
			this.nodes.dots = {};
			var _this = this;

			var wrapper = this.nodes.wrappers.dots
			            = $('<div></div>').addClass(this.class('wrappers.dots'));
			$(this.nodes.this).append(wrapper);

			this.slides.forEach(function(slide) {
				this.nodes.dots[slide.id] = $('<div></div>')
					.addClass(this.class('dot'))
					.attr('data-slide-id', slide.id)
					[this.settings.dotsHover ? 'hover' : 'click'](
						function() {_this.activate(slide.id);}
					)
					.appendTo(wrapper)[0];
			}, this);
		}
	};

	this.initSync = function() {
		var rawSync = this.settings.sync;
		rawSync = Array.isArray(rawSync)
				? rawSync
				: [rawSync];

		this.settings.sync = [];
		rawSync.forEach(function(rawSyncItem) {
			if (rawSyncItem instanceof Slider)
				this.settings.sync.push(rawSyncItem);
			else
				rawSyncItem = $(rawSyncItem).toArray()
					.forEach(function(element) {
						this.settings.sync.push(element.slider);
					}, this);
		}, this);

		this.settings.sync.forEach(function(slider) {
			slider.settings.sync.push(this);
		}, this);
	};

	this.newSlide = function(node) {
		var id = this.slides.length;
		var slide = new Slide(this, id, node);
		this.slides.push(slide)

		return slide;
	};

	function Slide(parent, id, node) {
		this.parent = parent;
		this.id = id;
		this.node = node;
		this.node.slide = this;

		$(node).addClass(this.parent.class('slide'));
		$(node).attr('data-slide-id', this.id);

		this.next = function() {
			var next = this.id + 1;

			if (next >= this.parent.slides.length)
				next = this.parent.settings.loop ? 0 : this.id;

			return this.parent.slides[next];
		};
		this.prev = function() {
			var prev = this.id - 1;

			if (prev < 0)
				prev = this.parent.settings.loop
			? this.parent.slides.length - 1
			: this.id;

			return this.parent.slides[prev];
		};
		this.activate = function() {
			return this.parent.activate(this.id);
		};
	};

//###########################
//### A D D I T I O N A L ###
//###########################

	this.class = function(name) {
		var addr = name.split('.');
		var _class = this.classes;
		var mod = '';
		addr.forEach(function(name, i) {
			if (i == addr.length - 1)
				[name, mod] = name.split('+');
			_class = _class[name];
		});
		return _class + (
			mod ? '--' + mod : ''
		);
	}

	this.select = function(name) {
		return '.' + this.class(name);
	}

//###########################
//### S E R V I C E #########
//###########################

	this._event = function(key, when, ...args) {
		if (this.settings.events.hasOwnProperty(key)
			&& this.settings.events[key].hasOwnProperty(when))
			return this.settings.events[key][when].apply(this, args);

		return args;
	}

	this._runner = function(key, func) {
		return function() {
			arguments = this._event(key, 'before', ...arguments);
			result = func.apply(this, Array.isArray(arguments)
				? arguments
				: [arguments]
			);
			this._event(key, 'after', arguments, result);

			return result;
		};
	};

	this._registryClosures = function() {
		for (var key in this) {
			if (this.hasOwnProperty(key)
				&& this[key]
				&& {}.toString.call(this[key]) === '[object Function]'
				&& key.charAt(0) !== '_'
			) {
				this[key] = this._runner(key, this[key]);
			}
		}
	};
	this._registryClosures();

	return this.init(node, ...args);

}
