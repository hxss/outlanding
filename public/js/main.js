
$(document).ready( function(){

	var slider = $('.clock__slider').slider({
		arrows: false,
		dots: false,
		events: {
			activate: {
				before: function(id) {
					var gradient = 'url(#linear' + id + ')';
					$('#circle--main').toggle().toggle().css('stroke', gradient);
					$('#circle--outside1').toggle().toggle().css('stroke', gradient);
					$('#circle--outside2').toggle().toggle().css('stroke', gradient);

					$('.projects').removeClass('projects--bg0 projects--bg1 projects--bg2')
					              .addClass('projects--bg' + id);

					return id;
				},
			},
		},
	})[0];

	setInterval(function() {
		slider.next();
	}, 6000);

	clockFontRecalc();
	$(window).resize(function() {
		clockFontRecalc();
	});

});

function clockFontRecalc() {
	var fz = ($(document).width() * 0.9 - 20) / 82;
	var fz2 = $('.clock').width() / 82;
	fz = fz > fz2 ? fz2 : fz;
	$('.clock').css('font-size', fz);
}

